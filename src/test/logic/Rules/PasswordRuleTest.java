package logic.Rules;

import domain.Person;
import domain.User;
import logic.RuleResult;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class PasswordRuleTest {

    @Test
    public void testCheckRule() throws Exception {
        PasswordRule rule = new PasswordRule();
        User user = new User();

        user.setPassword("123A4567");
        assertTrue(rule.checkRule(user).getResult() == RuleResult.Ok);

    }
    @Test
    public void testCheckRuleFalse() throws Exception {
        PasswordRule rule = new PasswordRule();
        User user = new User();

        user.setPassword("1234");
        assertTrue(rule.checkRule(user).getResult() == RuleResult.Error);

    }
}