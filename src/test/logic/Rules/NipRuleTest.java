package logic.Rules;

import domain.Person;
import logic.RuleResult;
import logic.Rules.NipRule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class NipRuleTest {

    @Test
    public void testCheckRule() throws Exception {
        NipRule rule = new NipRule();
        Person person =new Person();

        person.setNip(1234563218L);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Ok);

    }
    @Test
    public void testCheckRuleFalse() throws Exception {
        NipRule rule = new NipRule();
        Person person = new Person();

        person.setNip(95908L);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Error);

    }
    @Test
    public void testCheckRuleErr() throws Exception {
        NipRule rule = new NipRule();
        Person person = new Person();

        person.setNip(1234563218L);
        assertFalse(rule.checkRule(person).getResult() == RuleResult.Error);

    }


}