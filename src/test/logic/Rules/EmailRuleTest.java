package logic.Rules;

import domain.Person;
import logic.RuleResult;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class EmailRuleTest {
    @Test
    public void testCheckRule() throws Exception {
        EmailRule rule = new EmailRule();
        Person person =new Person();

        person.setEmail("ciastko@ciastko.pl");

        assertTrue(rule.checkRule(person).getResult() == RuleResult.Ok);

    }

    @Test
    public void testCheckRuleFalse() throws Exception {
        EmailRule rule = new EmailRule();
        Person person =new Person();

        person.setEmail("ciastkoiastko.polskanarodowosc.pl");

        assertTrue(rule.checkRule(person).getResult() == RuleResult.Error);

    }

}