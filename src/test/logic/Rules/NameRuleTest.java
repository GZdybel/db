package logic.Rules;

import domain.Person;
import logic.RuleResult;
import org.junit.Test;

import javax.lang.model.element.Name;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class NameRuleTest {
    @Test
    public void testCheckRule() throws Exception {
        NameRule rule = new NameRule();
        Person person =new Person();

        person.setFirstName("Ciastko");
        person.setSurname("Karmelowe");

        assertTrue(rule.checkRule(person).getResult() == RuleResult.Ok);

    }
    @Test
    public void testCheckRuleFalse() throws Exception {
        NameRule rule = new NameRule();
        Person person =new Person();

        person.setFirstName("");
        person.setSurname("");

        assertTrue(rule.checkRule(person).getResult() == RuleResult.Error);

    }

}