package logic.Rules;

import domain.Person;
import logic.RuleResult;
import logic.Rules.PeselRule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class PeselRuleTest {

    @Test
    public void testCheckRule() throws Exception {
        PeselRule rule = new PeselRule();
        Person person =new Person();

        person.setPesel(95011609908L);
        person.setDateOfBirth(1995, 01, 16);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Ok);

    }
    @Test
    public void testCheckRuleFalse() throws Exception {
        PeselRule rule = new PeselRule();
        Person person =new Person();

        person.setPesel(95908L);
        person.setDateOfBirth(1995, 5, 4);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Error);

    }

}