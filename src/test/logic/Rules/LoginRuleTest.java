package logic.Rules;

import domain.Person;
import domain.User;
import logic.RuleResult;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 27/11/15.
 */
public class LoginRuleTest {

    @Test
    public void testCheckRule() throws Exception {
            LoginRule rule = new LoginRule();
            User user = new User();

            user.setLogin("Ciastko");
            assertTrue(rule.checkRule(user).getResult() == RuleResult.Ok);

        }
        @Test
        public void testCheckRuleFalse() throws Exception {
            LoginRule rule = new LoginRule();
            User user = new User();

            user.setLogin("");
            assertTrue(rule.checkRule(user).getResult() == RuleResult.Error);

        }

}