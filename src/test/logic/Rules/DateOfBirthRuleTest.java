package logic.Rules;

import domain.Person;
import logic.RuleResult;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by FochMaiden on 26/11/15.
 */
public class DateOfBirthRuleTest {
    @Test
    public void testCheckRule() throws Exception {
        DateOfBirthRule rule = new DateOfBirthRule();
        Person person = new Person();

        person.setDateOfBirth(1995, 5, 3);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Ok);

    }

    @Test
    public void testCheckRuleFalse() throws Exception {
        DateOfBirthRule rule = new DateOfBirthRule();
        Person person = new Person();

        person.setDateOfBirth(2000, 5, 3);
        assertTrue(rule.checkRule(person).getResult() == RuleResult.Error);

    }

}