import domain.*;
import org.hibernate.Session;
import repository.Builder.EnumerationValuesBuilder;
import repository.Interface.EnumerationValueRepository;
import repository.Interface.RepositoryCatalog;
import repository.impl.*;
import unityofwork.HsqlUnitOfWorkRepository;
import unityofwork.UnitOfWork;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by FochMaiden on 25/11/15.
 */
public class Main {
    public static void main(String[] args) {


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager entityManager = emf.createEntityManager();
        Session session = (Session)entityManager.getDelegate();

        EnumerationValue enumerationValue = new EnumerationValue();
        enumerationValue.setValue("Ciastko");
        enumerationValue.setStringKey("OMFG");
        enumerationValue.setEnumerationName("Karmelowe");

        UnitOfWork unitOfWork= new UnitOfWork(session);
        RepositoryCatalog repositoryCatalog = new RepositoryCatalogImpl(unitOfWork);
        EntityBuilder entityBuilder = new EnumerationValuesBuilder();
        RepositoryImpl<EnumerationValue> repository;
        EnumerationValueRepository repo = repositoryCatalog.enumerations();
        HsqlUnitOfWorkRepository report = (HsqlUnitOfWorkRepository) repo;

        unitOfWork.markAsNew(enumerationValue, report);
        unitOfWork.saveChanges();
        unitOfWork.markAsDeleted(enumerationValue, report);
        unitOfWork.saveChanges();

        entityManager.close();
        emf.close();

    }

}
