package unityofwork;

import domain.Entity;

/**
 * Created by Gosia on 18/11/2015.
 */
public interface HsqlUnitOfWork {
    void saveChanges();
    void undo();
    void markAsNew(Entity entity, HsqlUnitOfWorkRepository repo);
    void markAsDeleted(Entity entity, HsqlUnitOfWorkRepository repo);
    void markAsChanged(Entity entity, HsqlUnitOfWorkRepository repo);
}
