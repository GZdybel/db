package unityofwork;

import domain.Entity;
import domain.EntityState;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Gosia on 18/11/2015.
 */
public class UnitOfWork implements HsqlUnitOfWork{


    private Session session;

    private Map<Entity, HsqlUnitOfWorkRepository> entities =
            new LinkedHashMap<Entity, HsqlUnitOfWorkRepository>();

    public UnitOfWork(Session session) {
        super();
        this.session = session;
    }

    public void saveChanges() {
        Transaction transaction = session.getTransaction();
        transaction.begin();
        for(Entity entity: entities.keySet())
        {
            switch(entity.getEntityState())
            {
                case Modified:
                    entities.get(entity).persistUpdate(entity);
                    break;
                case Deleted:
                    entities.get(entity).persistDelete(entity);
                    break;
                case New:
                    entities.get(entity).persistAdd(entity);
                    break;
                case UnChanged:
                    break;
                case Unknown:
                    break;}
        }

        try {
            transaction.commit();
            entities.clear();

        } catch (HibernateException e) {
            transaction.rollback();
            undo();
            e.printStackTrace();
        }
    }


    public void undo() {
        entities.clear();

    }

    public void markAsNew(Entity entity, HsqlUnitOfWorkRepository repository) {
        entity.setEntityState(EntityState.New);
        entities.put(entity, repository);

    }

    public void markAsChanged(Entity entity, HsqlUnitOfWorkRepository repository) {
        entity.setEntityState(EntityState.Modified);
        entities.put(entity, repository);

    }

    public void markAsDeleted(Entity entity, HsqlUnitOfWorkRepository repository) {
        entity.setEntityState(EntityState.Deleted);
        entities.put(entity, repository);

    }

    public  Session getSession() {
    return this.session;
    }
}
