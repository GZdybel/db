package unityofwork;

import domain.Entity;

/**
 * Created by Gosia on 18/11/2015.
 */
public interface HsqlUnitOfWorkRepository{
    void persistAdd(Entity entity);
    void persistUpdate(Entity entity);
    void persistDelete(Entity entity);
}
