package repository.impl;

import domain.Person;
import domain.User;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import repository.Interface.IPerson;
import repository.Interface.UserRepository;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public class PersonReopsitoryImpl  extends RepositoryImpl<Person> implements IPerson {
    Query query;

    public PersonReopsitoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }

    @Override
    void setUpdate(Person entity) throws HibernateException {
        session.update(entity.getPersonId());
        session.update(entity.getFirstName());
        session.update(entity.getSurname());
        session.update(entity.getPesel());
        session.update(entity.getNip());
        session.update(entity.getEmail());
        session.update(entity.getDateOfBirth());
    }

    @Override
    void setInsert(Person entity) throws HibernateException {
        session.persist(entity.getFirstName());
        session.persist(entity.getSurname());
        session.persist(entity.getPesel());
        session.persist(entity.getNip());
        session.persist(entity.getEmail());
        session.persist(entity.getDateOfBirth());
    }

    @Override
    String getTableName() {
        return "Person";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO PERSON (firstname, surname, pesel, nip, email, dateofbirth)" + "values(?,?,?,?,?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE PERSON SET(firstname, surname, pesel, nip, email, dateofbirth)=(?,?,?,?,?,?)";
    }
    public void withName(String firstName, String lastName) {
        String sql = "FROM Person WHERE firstName=: firstname and surname=: surname";
        query=session.createQuery(sql);
        query.setParameter("firstname", firstName);
        query.setParameter("surname", lastName);
        System.out.println(query.list());

    }

    public void withPeselAndDateOfBirth(long pesel, Date date) {
        String sql = "FROM Person WHERE pesel= :pesel and dateOfBirth =:dateofbirth";
        query=session.createQuery(sql);
        query.setParameter("pesel", pesel);
        query.setParameter("dateofbirth", date);
        System.out.println(query.list());
    }

    public void withUser(User user) {
        String sql = "FROM User WHERE person=:person";
        query=session.createQuery(sql);
        query.setParameter("person", user);
        System.out.println(query.list());
    }
    public List<Person> withPersonId(int id){
        String sql = "FROM Person WHERE personId=: personid";
        query=session.createQuery(sql);
        query.setParameter("personid", id);
        System.out.println(query.list());
        return query.list();
    }
}
