package repository.impl;

/**
 * Created by FochMaiden on 27/11/15.
 */

import org.hibernate.Session;
import repository.Builder.*;
import repository.Interface.*;
import unityofwork.UnitOfWork;

public class RepositoryCatalogImpl implements RepositoryCatalog {

    private Session session;
    private UnitOfWork unitOfWork;

    public RepositoryCatalogImpl(UnitOfWork unitOfWork) {
        super();
        this.session = unitOfWork.getSession();
        this.unitOfWork = unitOfWork;
    }

    public EnumerationValueRepository enumerations() {
        return new EnumerationValuesImpl(new EnumerationValuesBuilder(), unitOfWork);
    }

    public UserRepository getUsers() {
        return new UserRepositoryImpl(new UserBuilder(),unitOfWork);
    }

    public IAddress getAddresses() {
        return new AddressRepositoryImpl(new AddressBuilder(), unitOfWork);
    }

    public IPerson getPeople() {
        return new PersonReopsitoryImpl(new PersonBuilder(), unitOfWork);
    }

    public IPhoneNumber getNumbers() {
        return new PhoneNumberRepositoryImpl(new PhoneNumberBuilder(), unitOfWork);
    }

    public IRolePermissions getRolePermi() {
        return new RolePermissionsRepositoryImpl(new RolePermissionsBuilder(), unitOfWork);
    }

    public IUserRoles getUserRoles() {
        return new UserRolesImpl(new UserRolesBuilder(), unitOfWork);
    }

    public void commit() {
        unitOfWork.saveChanges();
    }
}
