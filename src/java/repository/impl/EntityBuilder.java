package repository.impl;

import domain.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 28/11/15.
 */
public interface EntityBuilder<TEntity extends Entity> {

    TEntity build(ResultSet rs) throws SQLException;

}