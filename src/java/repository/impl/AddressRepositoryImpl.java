package repository.impl;

import domain.Address;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import repository.Interface.IAddress;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public class AddressRepositoryImpl extends RepositoryImpl<Address>implements IAddress {
    Query query;

    public AddressRepositoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }

    @Override
    void setUpdate(Address entity) throws HibernateException {
        session.update(entity.getAddressId());
        session.update(entity.getCountryId());
        session.update(entity.getRegionId());
        session.update(entity.getCity());
        session.update(entity.getStreet());
        session.update(entity.getHouseNumber());
        session.update(entity.getLocalNumber());
        session.update(entity.getZipCode());
        session.update(entity.getTypeId());
    }

    @Override
    void setInsert(Address entity) throws HibernateException {
        session.persist(entity.getCountryId());
        session.persist(entity.getRegionId());
        session.persist(entity.getCity());
        session.persist(entity.getStreet());
        session.persist(entity.getHouseNumber());
        session.persist(entity.getLocalNumber());
        session.persist(entity.getZipCode());
        session.persist(entity.getTypeId());
    }

    @Override
    String getTableName() {
        return "Address";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO Address(CountryId, RegionId, City, Street HouseNumber, LocalNumber, ZipCode, typeid)" + "Values(?,?,?,?,?,?,?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "Update address set(CountryId, RegionId, City, Street HouseNumber, LocalNumber, ZipCode, typeid)=(?,?,?,?,?,?,?)";
    }

    public void withCity(String city) {
        String sql = "FROM Address WHERE city = :city";
        query=session.createQuery(sql);
        query.setParameter("city", city);
        System.out.println(query.list());
    }

    public List withAddressId(int id) {
        String sql = "FROM Address WHERE addressId=:addressid";
        query=session.createQuery(sql);
        query.setParameter("addressid", id);
       return query.list();
    }



    public void withStreetAndHouseNumber(String street, String houseNumber) {
        String sql = "FROM Address WHERE street=:street and houseNumber=:houseNumber ";
        query=session.createQuery(sql);
        query.setParameter("street", street);
        query.setParameter("houseNumber", houseNumber);
        System.out.println(query.list());

    }
}
