package repository.impl;

import domain.User;
import domain.UserRoles;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import repository.Interface.IUserRoles;
import unityofwork.UnitOfWork;

import java.sql.SQLException;

/**
 * Created by FochMaiden on 03/12/15.
 */
public class UserRolesImpl extends RepositoryImpl<UserRoles> implements IUserRoles {
    Query query;
    public UserRolesImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }

    @Override
    void setUpdate(UserRoles entity) throws HibernateException {
        session.update(entity.getUserRolesId());
        session.update(entity.getUserId());
        session.update(entity.getRoleId());
    }

    @Override
    void setInsert(UserRoles entity) throws HibernateException {
        session.persist(entity.getUserId());
        session.persist(entity.getRoleId());
    }

    @Override
    String getTableName() {
        return "Userroles";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO Userroles(userid, roleid)"
                + "VALUES(?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE Userroles SET (userid, roleid)=(?,?) WHERE id=?";
    }

    public void withUserRolesId(int id) {
        String sql = "FROM UserRoles WHERE userRolesId=:userrolesid ";
        query=session.createQuery(sql);
        query.setParameter("userrolesid", id);
        System.out.println(query.list());

    }

    public void withUserAndRole(int userId, int roleId) {
        String sql = "FROM UserRoles WHERE userId=:userid and roleId=:roleid";
        query=session.createQuery(sql);
        query.setParameter("userid", userId);
        query.setParameter("roleid", roleId);
        System.out.println(query.list());
    }

    public void withUser(User user) {
        String sql = "FROM User WHERE userRoles=: userroles ";
        query=session.createQuery(sql);
        query.setParameter("userroles", user);
        System.out.println(query.list());
    }
}
