package repository.impl;

import domain.User;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import repository.Interface.UserRepository;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by FochMaiden on 27/11/15.
 */

public class UserRepositoryImpl extends RepositoryImpl<User> implements UserRepository {
    Query query;

    public UserRepositoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }

    @Override
    void setUpdate(User entity) throws HibernateException {
        session.update(entity.getLogin());
        session.update(entity.getPassword());
        session.update(entity.getId());
    }

    @Override
    void setInsert(User entity) throws HibernateException {
        session.persist(entity.getLogin());
        session.persist(entity.getPassword());
    }

    @Override
    String getTableName() {
        return "User";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO user(login,password)"
                + "VALUES(?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE user SET (login,password)=(?,?) WHERE id=?";
    }

    public List<User> withLogin(String login) {
        String sql = "FROM User WHERE login = :login";
        query=session.createQuery(sql);
        query.setParameter("login",login);
        return query.list();
    }

    public List<User> withLoginAndPassword(String login, String password) {
        String sql = "FROM User WHERE login = :login AND password = :password";
        query=session.createQuery(sql);
        query.setParameter("login",login);
        query.setParameter("password", password);
        return query.list();
    }

    public void setupPermissions(User user) {



    }

}