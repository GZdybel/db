package repository.impl;

import domain.RolePermissions;
import domain.User;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import repository.Interface.IRolePermissions;
import unityofwork.UnitOfWork;

import java.sql.SQLException;

/**
 * Created by FochMaiden on 03/12/15.
 */
public class RolePermissionsRepositoryImpl extends RepositoryImpl<RolePermissions> implements IRolePermissions {
    Query query;
    public RolePermissionsRepositoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }


    @Override
    void setUpdate(RolePermissions entity) throws HibernateException {
        session.update(entity.getRoleId());
        session.update(entity.getPermissionId());
    }

    @Override
    void setInsert(RolePermissions entity) throws HibernateException{
        session.persist(entity.getPermissionId());
    }

    @Override
    String getTableName() {
        return "Rolepermissions";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO rolepermissions(permissionid)" + "values(?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE rolepermissions SET (permissionid)=(?) WHERE id=?";
    }

    public void withRoleId(int id) {
        String sql = "FROM RolePermissions WHERE roleId=:roleid";
        query=session.createQuery(sql);
        query.setParameter("roleid", id);
        System.out.println(query.list());

    }

    public void withRoleAndPermission(int id, int permissionId) {
        String sql = "FROM RolePermissions WHERE roleId=:roleid AND permissionId=: permissionid";
        query=session.createQuery(sql);
        query.setParameter("roleid", id);
        query.setParameter("permissionid", permissionId);
        System.out.println(query.list());

    }

    public void withUser(User user) {
        String sql = "FROM User WHERE userRoles=: userroles ";
        query=session.createQuery(sql);
        query.setParameter("userroles", user);
        System.out.println(query.list());

    }
}
