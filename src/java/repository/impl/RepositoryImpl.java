package repository.impl;

import domain.Entity;
import org.hibernate.Session;
import repository.PagingInfo;
import repository.Interface.Repository;
import unityofwork.HsqlUnitOfWorkRepository;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FochMaiden on 28/11/15.
 */
public abstract class RepositoryImpl<TEntity extends Entity> implements Repository<TEntity>, HsqlUnitOfWorkRepository {

    Session session;
    UnitOfWork unitOfWork;
    EntityBuilder entityBuilder;

    String insert = getInsertQuery();
    String delete = "Delete * from " + getTableName() + "where id= : ciastko_id" ;
    String update= getUpdateQuery();
    String selectAll = "Select * from" + getTableName();
    String withId = "Select * from " + getTableName() + "where id= : ciastko_id ";

    org.hibernate.Query withid ;

    public RepositoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        this.session = unitOfWork.getSession();
        this.entityBuilder = entityBuilder;
        this.unitOfWork = unitOfWork;


    }

    public void add(TEntity entitiy) {
        unitOfWork.markAsNew(entitiy, this);
    }

    public void delete(TEntity entitiy) {
        unitOfWork.markAsDeleted(entitiy, this);

    }

    public void modify(TEntity entitiy) {
        unitOfWork.markAsChanged(entitiy, this);
    }

    public void persistAdd(Entity entity) {

        session.persist(entity);

    }

    public void persistUpdate(Entity entity) {
       session.update(entity);
    }

    public void persistDelete(Entity entity) {
        session.delete(entity);
    }

    public List<TEntity> withId(int id) {
        withid =  session.createQuery(withId);
        List<TEntity> result = new ArrayList<TEntity>();
        withid.setParameter("ciastko_id", id);
    return result = withid.list();
    }

    public void allOnPage(PagingInfo page) {

    }


    public void count() {

    }

    abstract void setUpdate(TEntity entity) throws SQLException;
    abstract void setInsert(TEntity entity) throws SQLException;
    abstract String getTableName();
    abstract String getInsertQuery();
    abstract String getUpdateQuery();
}
