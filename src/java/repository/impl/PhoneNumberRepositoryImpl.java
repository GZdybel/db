package repository.impl;

import domain.Person;
import domain.PhoneNumber;
import org.hibernate.Query;
import repository.Interface.IPhoneNumber;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public class PhoneNumberRepositoryImpl extends RepositoryImpl<PhoneNumber> implements IPhoneNumber{
    Query query;
    public PhoneNumberRepositoryImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }
    @Override
         void setUpdate(PhoneNumber entity) throws SQLException {
        session.update(entity.getPhoneNumberid());
        session.update(entity.getCountryPrefix());
        session.update(entity.getCityPrefix());
        session.update(entity.getNumber());
        session.update(entity.getTypeId());
    }

    @Override
    void setInsert(PhoneNumber entity) throws SQLException {
        session.persist(entity.getCountryPrefix());
        session.persist(entity.getCityPrefix());
        session.persist(entity.getNumber());
        session.persist(entity.getTypeId());
    }

    @Override
    String getTableName() {
        return "Phonenumber";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO Phonenumber(countryprefix, cityprefix, number, typeid)" + "Values(?,?,?,?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE PHONENUMBER SET(countryprefix, cityprefix, number, typeid)=(?,?,?,?,?)";
    }

    public List withNumberId(String id) {
        String sql = "FROM PhoneNumber WHERE phoneNumberid=:phonenumberid";
        query=session.createQuery(sql);
        query.setParameter("phonenumberid", id);
        return query.list();
    }

    public void withNumber(int number) {
        String sql = "FROM PhoneNumber WHERE number=:phonenumber";
        query=session.createQuery(sql);
        query.setParameter("phonenumber", number);
        System.out.println(query.list());

    }

    public void withPerson(Person person) {
        String sql = "FROM Person WHERE phoneNumber=:phonenumber ";
        query=session.createQuery(sql);
        query.setParameter("phonenumber", person);
        System.out.println(query.list());
    }


}
