package repository.impl;

import domain.EnumerationValue;
import org.hibernate.Query;
import repository.Interface.EnumerationValueRepository;
import unityofwork.HsqlUnitOfWorkRepository;
import unityofwork.UnitOfWork;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by FochMaiden on 27/11/15.
 */
public class EnumerationValuesImpl extends RepositoryImpl<EnumerationValue> implements EnumerationValueRepository, HsqlUnitOfWorkRepository{
    Query query;
    public EnumerationValuesImpl(EntityBuilder entityBuilder, UnitOfWork unitOfWork) {
        super(entityBuilder, unitOfWork);
    }

    @Override
    void setUpdate(EnumerationValue entity) throws SQLException {
        session.update(entity.getId());
        session.update(entity.getIntKey());
        session.update(entity.getStringKey());
        session.update(entity.getValue());
        session.update(entity.getEnumerationName());
    }

    @Override
    void setInsert(EnumerationValue entity) throws SQLException {
        session.persist(entity.getIntKey());
        session.persist(entity.getStringKey());
        session.persist(entity.getValue());
        session.persist(entity.getEnumerationName());
    }

    @Override
    String getTableName() {
        return "EnumerationValue";
    }

    @Override
    String getInsertQuery() {
        return "INSERT INTO EnumerationValue(intkey,stringKey, value, enumerationName)"
                +"VALUES(?,?,?,?)";
    }

    @Override
    String getUpdateQuery() {
        return "UPDATE EnumerationValue SET(intkey, stringKey, value, enumerationName)";
    }


    public void withName(String name) {
        String sql = "FROM EnumerationValue WHERE enumerationName=: enumerationName";
        query=session.createQuery(sql);
        query.setParameter("enumerationName", name);
        System.out.println(query.list());

    }

    public void withIntKey(int key, String name) {
        String sql = "FROM EnumerationValue WHERE intKey =:intKey and enumerationName =:enumerationName";
        query=session.createQuery(sql);
        query.setParameter("intKey", key);
        query.setParameter("enumerationName", name);
        System.out.println(query.list());
    }

    public void withStringKey(String key, String name) {
        String sql = "FROM EnumerationValue WHERE enumerationName =:enumerationName and stringKey =:stringKey";
        query=session.createQuery(sql);
        query.setParameter("stringKey", key);
        query.setParameter("enumerationName", name);
        System.out.println(query.list());
    }

    public List<EnumerationValue> getAll() {
        return null;
    }
}
