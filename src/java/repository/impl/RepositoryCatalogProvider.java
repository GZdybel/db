package repository.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import unityofwork.UnitOfWork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 28/11/15.
 */
public class RepositoryCatalogProvider {

    public static RepositoryCatalogImpl catalog(UnitOfWork uow)
    {

        try {
            RepositoryCatalogImpl catalog = new RepositoryCatalogImpl(uow);

            return catalog;
        } catch (HibernateException e) {

            e.printStackTrace();
        }
        return null;
    }
}
