package repository.Builder;

import domain.User;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 28/11/15.
 */
public class UserBuilder implements EntityBuilder<User> {

    public User build(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));

        return user;
    }
}
