package repository.Builder;

import domain.PhoneNumber;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 04/12/15.
 */
public class PhoneNumberBuilder implements EntityBuilder<PhoneNumber> {
    public PhoneNumber build(ResultSet rs) throws SQLException {
        PhoneNumber pn = new PhoneNumber();
        pn.setPhoneNumberid(rs.getInt("phoneNumerId"));
        pn.setCountryPrefix(rs.getInt("countryPrefix"));
        pn.setCityPrefix(rs.getInt("cityPrefix"));
        pn.setNumber(rs.getInt("number"));
        pn.setTypeId(rs.getInt("typeId"));

        return pn;
    }
}
