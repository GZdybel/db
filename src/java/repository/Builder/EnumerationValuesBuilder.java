package repository.Builder;

import domain.EnumerationValue;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 28/11/15.
 */
public class EnumerationValuesBuilder implements EntityBuilder<EnumerationValue> {
    public EnumerationValue build(ResultSet rs) throws SQLException {
        EnumerationValue eu = new EnumerationValue();
        eu.setId(rs.getInt("id"));
        eu.setIntKey(rs.getInt("IntKey"));
        eu.setStringKey(rs.getString("StringKey"));
        eu.setValue(rs.getString("Value"));
        eu.setEnumerationName(rs.getString("EnumerationValues"));
        return eu;
    }
}
