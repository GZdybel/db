package repository.Builder;

import domain.Person;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by FochMaiden on 04/12/15.
 */
public class PersonBuilder implements EntityBuilder<Person> {
    public Person build(ResultSet rs) throws SQLException {
        Person person = new Person();
        person.setPersonId(rs.getInt("personId"));
        person.setFirstName(rs.getString("firstName"));
        person.setSurname(rs.getString("lastName"));
        person.setPesel(rs.getLong("pesel"));
        person.setNip(rs.getLong("nip"));
        person.setEmail(rs.getString("email"));
//        person.setDateOfBirth(rs.getDate("dateOfBirth"));

        return person;
    }
}
