package repository.Builder;

import domain.RolePermissions;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 04/12/15.
 */
public class RolePermissionsBuilder implements EntityBuilder<RolePermissions> {
    public RolePermissions build(ResultSet rs) throws SQLException {
        RolePermissions rp = new RolePermissions();
        rp.setRoleId(rs.getInt("roleId"));
        rp.setPermissionId(rs.getInt("permissionId"));


        return rp;
    }
}
