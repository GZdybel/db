package repository.Builder;

import domain.Address;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 04/12/15.
 */
public class AddressBuilder implements EntityBuilder<Address> {
    public Address build(ResultSet rs) throws SQLException {
        Address address = new Address();
        address.setAddressId(rs.getInt("addressId"));
        address.setCountryId(rs.getInt("countryId"));
        address.setRegionId(rs.getInt("regionId"));
        address.setCity(rs.getString("city"));
        address.setHouseNumber(rs.getInt("houseNumber"));
        address.setLocalNumber(rs.getInt("localNumber"));
        address.setZipCode(rs.getString("zipcode"));
        address.setTypeId(rs.getInt("typeId"));
        return address;
    }
}
