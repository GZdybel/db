package repository.Builder;

import domain.UserRoles;
import repository.impl.EntityBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by FochMaiden on 04/12/15.
 */
public class UserRolesBuilder implements EntityBuilder<UserRoles> {
    public UserRoles build(ResultSet rs) throws SQLException {
        UserRoles ur = new UserRoles();
        ur.setUserRolesId(rs.getInt("userRolesId"));
        ur.setRoleId(rs.getInt("roleId"));
        ur.setUserId(rs.getInt("userId"));
        return ur;
    }
}
