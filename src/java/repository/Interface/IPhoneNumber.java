package repository.Interface;

import domain.Person;
import domain.PhoneNumber;

import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public interface IPhoneNumber {
    List withNumberId(String id);
    void withNumber(int number);
    void withPerson(Person person);
}
