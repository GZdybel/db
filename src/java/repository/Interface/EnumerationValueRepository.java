package repository.Interface;

import domain.EnumerationValue;
import repository.Interface.Repository;

/**
 * Created by Gosia on 04/11/2015.
 */
public interface EnumerationValueRepository extends Repository<EnumerationValue> {
    void withName(String name);
    void withIntKey(int key, String name);
    void withStringKey(String key, String name);
}
