package repository.Interface;

import domain.User;

/**
 * Created by FochMaiden on 02/12/15.
 */
public interface IUserRoles {
    void withUserRolesId(int id);
    void withUserAndRole(int userId, int roleId);
    void withUser(User user);
}
