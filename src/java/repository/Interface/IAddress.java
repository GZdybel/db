package repository.Interface;

import domain.Address;

import java.util.AbstractCollection;
import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public interface IAddress {
    void withCity(String city);
    List withAddressId(int id);
    void withStreetAndHouseNumber(String street, String houseNumber);
}
