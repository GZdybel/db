package repository.Interface;

import domain.User;

import java.util.List;

/**
 * Created by Gosia on 04/11/2015.
 */
public interface UserRepository extends Repository<User> {
    List<User> withLogin(String login);
    List<User> withLoginAndPassword(String login, String password);
    void setupPermissions(User user);
}
