package repository.Interface;

import domain.User;

/**
 * Created by FochMaiden on 02/12/15.
 */
public interface IRolePermissions {
    void withRoleId(int id);
    void withRoleAndPermission(int id, int permissionId);
    void withUser(User user);

}
