package repository.Interface;

import domain.Person;
import domain.User;

import java.util.Date;
import java.util.List;

/**
 * Created by FochMaiden on 02/12/15.
 */
public interface IPerson {
    void withName(String firstName, String lastName);
    void withPeselAndDateOfBirth(long pesel, Date date);
    List withPersonId(int id);
    void withUser(User user);

}
