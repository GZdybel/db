package repository.Interface;

import repository.PagingInfo;

import java.util.List;

/**
 * Created by Gosia on 04/11/2015.
 */
public interface Repository<TEntity> {
    List<TEntity> withId(int id);
    void allOnPage(PagingInfo page);
    void add(TEntity entitiy);
    void delete(TEntity entitiy);
    void modify(TEntity entitiy);
    void count();

    List<TEntity> getAll();

}
