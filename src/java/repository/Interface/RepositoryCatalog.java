package repository.Interface;

/**
 * Created by Gosia on 04/11/2015.
 */
public interface RepositoryCatalog {
    UserRepository getUsers();
    EnumerationValueRepository enumerations();
    IAddress getAddresses();
    IPerson getPeople();
    IPhoneNumber getNumbers();
    IRolePermissions getRolePermi();
    IUserRoles getUserRoles();

}
