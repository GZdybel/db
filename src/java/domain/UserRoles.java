package domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Gosia on 04/11/2015.
 */
@javax.persistence.Entity
public class UserRoles extends Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userRolesId;
    private int userId;
    private int roleId;

    @OneToMany
    private List<User> userList;

    @OneToMany
    private List<RolePermissions> permissions;

    public int getUserRolesId() {
        return userRolesId;
    }

    public void setUserRolesId(int userRolesId) {
        this.userRolesId = userRolesId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<RolePermissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<RolePermissions> permissions) {
        this.permissions = permissions;
    }
}
