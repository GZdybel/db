package domain;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gosia on 04/11/2015.
 */
@javax.persistence.Entity
public class User extends Entity {
    @Id
    private String login;
    private String password;

    @OneToOne
    private Person person;

    @OneToMany
    private List<UserRoles> userRoles;


    public User() {
        super();
        userRoles = new ArrayList<UserRoles>();
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRoles> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoles> userRoles) {
        this.userRoles = userRoles;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
        if(!this.equals(person.getUser()))
            person.setUser(this);
    }
}
