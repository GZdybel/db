package domain;

/**
 * Created by Gosia on 04/11/2015.
 */
public enum EntityState {
    New, Modified, UnChanged, Deleted, Unknown
}
