package domain;

import javax.persistence.*;

/**
 * Created by FochMaiden on 25/11/15.
 */
@javax.persistence.Entity
public class PhoneNumber extends Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int phoneNumberid;
    private int countryPrefix;
    private int cityPrefix;
    private int number;
    private int typeId;

    @OneToOne
    private Person person;

    public int getPhoneNumberid() {
        return phoneNumberid;
    }

    public void setPhoneNumberid(int phoneNumberid) {
        this.phoneNumberid = phoneNumberid;
    }

    public int getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(int countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public int getCityPrefix() {
        return cityPrefix;
    }

    public void setCityPrefix(int cityPrefix) {
        this.cityPrefix = cityPrefix;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
