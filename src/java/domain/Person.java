package domain;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Gosia on 18/11/2015.
 */
@javax.persistence.Entity
public class Person extends Entity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int personId;

    private String firstName;
    private String surname;
    private long pesel;
    private long nip;
    private String email;
    private Date dateOfBirth;
    @OneToOne
    private User user;
    @OneToMany
    private List<Address> address;
    @OneToMany
    private List<PhoneNumber> phoneNumber;

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getPesel() {
        return pesel;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    public long getNip() {
        return nip;
    }

    public void setNip(long nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month, day);
       this.dateOfBirth = cal.getTime();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if(!this.equals(user.getPerson()))
            user.setPerson(this);
        this.user = user;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public List<PhoneNumber> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
