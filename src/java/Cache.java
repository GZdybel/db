import domain.EnumerationValue;
import org.hibernate.Session;
import repository.Interface.Repository;
import repository.Interface.RepositoryCatalog;
import repository.impl.RepositoryCatalogImpl;
import unityofwork.UnitOfWork;

import java.util.*;

/**
 * Created by FochMaiden on 04/12/15.
 */
public class Cache {
    private Timer timer = new Timer();
    private static Cache cache;
    private static Object token = new Object();
    private Map<Integer, String> map = new HashMap<Integer, String>();


    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            resetCache();
        }
    };

    private void setAutoRefreshingOfCache(){
        timer.schedule(timerTask,5000L);
    }

    private Cache(){
        setAutoRefreshingOfCache();
    }

    public static Cache getInstance(){

        if(cache == null){
            synchronized(token)
            {
                if(cache==null) {
                    cache = new Cache();
                }

            }
        }
        return cache;
    }

    public static void resetCache(){
        if(cache != null){
            synchronized (token){
                if (cache!=null){
                    cache = new Cache();
                }
            }
        }else {
            getInstance();
        }
    }

    private void getCiastko(){
        Session session;
        UnitOfWork unitOfWork = new UnitOfWork(session);
        RepositoryCatalog repositoryCatalog = new RepositoryCatalogImpl(unitOfWork);
        Repository<EnumerationValue> enumerationValueRepository = repositoryCatalog.enumerations();
        List<EnumerationValue> enumerationValues = enumerationValueRepository.getAll();
        int i = 0;
        for(EnumerationValue c : enumerationValues ){
            map.put(i,c.getEnumerationName());
            i++;
        }

    }


}
