package logic;

/**
 * Created by Gosia on 18/11/2015.
 */
public interface CheckRule<TEntity> {
    CheckResult checkRule(TEntity entity);
}
