package logic.Rules;

import domain.User;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

/**
 * Created by FochMaiden on 27/11/15.
 */
public class LoginRule implements CheckRule<User>{
    private String login;
    private boolean isValid = true;

    public CheckResult checkRule(User user) {
        CheckResult result = null;
        this.login = user.getLogin();
        validLogin();

        if(isValid){
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please give proper Login" , RuleResult.Error);
        }
        return result;
    }

    private void validLogin() {
        if(login == null || login ==""){
            this.isValid = false;
        }

    }
}
