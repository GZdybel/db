package logic.Rules;

import domain.Person;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

/**
 * Created by Gosia on 18/11/2015.
 */
public class EmailRule implements CheckRule<Person> {
    private String email;
    String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    private boolean isValid;

    public CheckResult checkRule(Person person){
        CheckResult result = null;
        this.email = person.getEmail();
        isValid();

        if(isValid){
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("", RuleResult.Error);
        }

        return result;
    }

    private void isValid() {
        isValid = email.matches(regex);
    }
}
