package logic.Rules;

import domain.Person;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Gosia on 18/11/2015.
 */
public class DateOfBirthRule implements CheckRule<Person> {
    private Date dateOfBirth;
    private Calendar yearOfBirth = new GregorianCalendar();
    private Calendar todayDate = Calendar.getInstance();

    private boolean isValid = true;


    public CheckResult checkRule(Person person) {
        CheckResult result = null;
        this.dateOfBirth = person.getDateOfBirth();
        yearOfBirth.setTime(dateOfBirth);
        isAdult();
        isDead();


        if(isValid){
            result = new CheckResult("" , RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please select valid date", RuleResult.Error);
        }
        return result;
    }


    private void isDead() {
        if(todayDate.get(Calendar.YEAR) - yearOfBirth.get(Calendar.YEAR) > 122){
            isValid = false;
        }

    }

    private void isAdult() {
        if(todayDate.get(Calendar.YEAR) - yearOfBirth.get(Calendar.YEAR) <= 18 ){
            isValid = false;
        }

    }
}
