package logic.Rules;

import domain.Person;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

import java.util.Objects;

/**
 * Created by Gosia on 18/11/2015.
 */
public class NameRule implements CheckRule<Person> {
    private String firstName;
    private String lastName;

    private boolean isValid = true;

    public CheckResult checkRule(Person person) {
        CheckResult result = null;
        this.firstName = person.getFirstName();
        this.lastName = person.getSurname();
        validFirstName();
        validLastName();

        if(isValid) {
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please give proper Name", RuleResult.Error);
        }

        return result;
    }

    private void validFirstName() {
        if(firstName == null || Objects.equals(firstName, "")) {
            isValid = false;
        }
    }
    private void validLastName() {
        if(lastName == null || Objects.equals(lastName, "")){
            isValid = false;
        }
    }

}



