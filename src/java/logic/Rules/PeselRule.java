package logic.Rules;

import domain.Person;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Gosia on 18/11/2015.
 */
public class PeselRule implements CheckRule<Person>{
    private Person person;
    private long pesel;
    private int[] peselWage = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
    private boolean isValid = true;
    int sum = 0;

    public CheckResult checkRule(Person person) {
        CheckResult result = null;
        this.person = person;
        this.pesel = person.getPesel();

        validPeselNumber();

        if(isValid){
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please give proper Pesel" , RuleResult.Error);
        }
        return result;
    }

    private void validPeselNumber(){
        validPeselLength();
        if (isValid) { matchesBirthDate();
        validPeselControlNumber();}
    }

    private void validPeselControlNumber() {
        for(int i=0; i < 10; i++){
            sum+= peselWage[i] * Integer.parseInt(String.valueOf(pesel).substring(i, i + 1));
        }
        sum=sum%10;
        if(sum==10){
            this.sum =0;
        }else{
            sum = 10-sum;
        }


        isValid = Integer.parseInt(String.valueOf(pesel).substring(10)) == sum;
    }

    private void validPeselLength() {
        if(String.valueOf(pesel).length()!=11){
            isValid = false;
        }
    }

    private void matchesBirthDate(){
        int counter = 0;
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(person.getDateOfBirth());
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH));
        if(month.length()==1){
            month="0"+month;
        }
        String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        if(day.length()==1){
            day="0"+day;
        }
        String dob = year.substring(2,4)+month.substring(0, 2)+day.substring(0, 2);

        for(int i=0; i < 6; i++ ){
            if(dob.charAt(i) == String.valueOf(pesel).charAt(i)){
                counter++;
            }
        }
        if(counter == 6){
            isValid=true;
        }
        else isValid=false;



    }

}
