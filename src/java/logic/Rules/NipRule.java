package logic.Rules;

import domain.Person;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

/**
 * Created by Gosia on 18/11/2015.
 */
public class NipRule implements CheckRule<Person> {
    private long nip;
    private int[] nipWage = {6, 5, 7, 2, 3, 4, 5, 6, 7};
    private boolean isValid = true;
    private int sum;

    public CheckResult checkRule(Person person) {
        CheckResult result = null;
        this.nip = person.getNip();
        validNip();
        if (isValid){validNipControlNumber();}

        if(isValid){
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please give proper Nip" , RuleResult.Error);
        }
    return result;
    }

    private void validNip() {
        if (String.valueOf(nip).length() != 10) {
            isValid = false;
        }
    }
    private void validNipControlNumber() {

        for (int i = 0; i < 9; i++) {
            sum += nipWage[i] * Integer.parseInt(String.valueOf(nip).substring(i, i+1));
        }
        sum = sum%11;

        isValid = Integer.parseInt(String.valueOf(nip).substring(9)) == sum;
    }
}

