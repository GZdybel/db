package logic.Rules;

import domain.User;
import logic.CheckResult;
import logic.CheckRule;
import logic.RuleResult;

/**
 * Created by Gosia on 18/11/2015.
 */
public class PasswordRule implements CheckRule<User> {
    private String password;
    private boolean isValid = true;

    public CheckResult checkRule(User user) {
        CheckResult result = null;
        this.password = user.getPassword();

        validPasswordStrength();

        if(isValid){
            result = new CheckResult("", RuleResult.Ok);
        }
        else {
            result = new CheckResult("Please give proper Password" , RuleResult.Error);
        }
        return result;
    }

    private void validPasswordStrength(){
        validPasswordLength();
        int counterUpperCase = 0;
        int counterDigit = 0;
        if(isValid){
            for(int i=0; i< password.length(); i++){
                if(Character.isUpperCase(password.charAt(i))){
                    counterUpperCase ++;
                }
                if(Character.isDigit(password.charAt(i))){
                    counterDigit++;
                }
            }

            if(counterDigit >=1 && counterUpperCase >=1){
                isValid = true;
            }

            else isValid = false;

        }
    }

    private void validPasswordLength() {
        if(password.length()<=6){
            this.isValid = false;
        }

    }
}
