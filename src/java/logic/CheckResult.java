package logic;

/**
 * Created by Gosia on 18/11/2015.
 */
public class CheckResult {
    private String message;
    private RuleResult result;

    public CheckResult(String message, RuleResult result ) {
        this.result = result;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RuleResult getResult() {
        return result;
    }

    public void setResult(RuleResult result) {
        this.result = result;
    }
}
